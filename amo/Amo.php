<?php

namespace amo;

/**
 * Class Amo
 * Attention! The class is not designed to process arrays of entities, subject to API restrictions
 * There is no token refresh function
 * For the test only. There is a ready-made solution https://github.com/amocrm/amocrm-api-php
 * @package amo
 */
class Amo
{
    private $base_url;
    private $access_token;
    private $client;

    /**
     * Amo constructor.
     * @param string $base_url
     */
    public function __construct($base_url)
    {
        $this->base_url = $base_url;
        $this->client = new \GuzzleHttp\Client(['base_uri' => $this->base_url]);
        if (is_file(__DIR__ . '/tokens.json')) {
            $file = json_decode(file_get_contents(__DIR__ . '/tokens.json'), 1);
            $token = !empty($file['access_token']) ? $file['access_token'] : '';
            $type = !empty($file['token_type']) ? $file['token_type'] : '';
            $this->access_token = $type . ' ' . $token;
        }
    }

    /**
     * To get a token
     * example $data
     * ```pnp
     *  $data = [
     *      "client_secret" => "xxxxxxxxx",
     *      "client_id" => "xxxxxxx",
     *      "grant_type" => "authorization_code",
     *      "code" => 'xxxxxxx',
     *      "redirect_uri" => "http://my-host.com/xxxxxx",
     *  ];
     * ```
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function writeToken(array $data)
    {
        if ($this->access_token) {
            return ['error' => 'access_token exist'];
        }
        $response = $this->request('oauth2/access_token', $data);
        if (empty($response['error'])) {
            return file_put_contents(__DIR__ . '/tokens.json', \GuzzleHttp\json_encode($response));
        } else {
            return $response;
        }
    }

    /**
     * Request to Amo server
     * @param string $url
     * @param array $data
     * @param string $method
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request($url, $data, $method = 'POST')
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];
        if ($url != 'oauth2/access_token' && $this->access_token) {
            $headers = array_merge($headers, ['Authorization' => $this->access_token]);
        }
        $query = [
            'headers' => $headers,
        ];
        switch ($method) {
            case 'GET':
                $query = array_merge($query, ['query' => http_build_query($data)]);
                break;
            case 'POST':
                $query = array_merge($query, ['body' => \GuzzleHttp\json_encode($data)]);
                break;
            default:
                throw new \Exception('Method ' . $method . ' not supported');
        }
        try {
            $response = $this->client->request($method, $url, $query);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return ['error' => true, 'message' => \GuzzleHttp\json_decode($e->getResponse()->getBody()->getContents(), 1), 'code' => $e->getResponse()->getStatusCode()];
        }
        return \GuzzleHttp\json_decode($response->getBody()->getContents(), 1);
    }

    /**
     * Add lead
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addLeads(array $data)
    {
        $data = $this->checkData($data, 'name');
        $leads['add'] = $data;
        return $this->request('api/v2/leads', $leads);
    }

    /**
     * Add contact
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addContacts(array $data)
    {
        $data = $this->checkData($data, 'name');
        $contacts['add'] = $data;
        return $this->request('api/v2/contacts', $contacts);
    }


    /**
     * Update one contact
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateContacts(array $data)
    {
        $data = $this->checkData($data);
        $contacts['update'] = $data;
        return $this->request('api/v2/contacts', $contacts);
    }

    /**
     * Update one lead
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateLead(array $data)
    {
        $data = $this->checkData($data);
        $id = array_map(function ($el) {
            return $el['id'];
        }, $data);

        $id = $id ? reset($id) : null;
        if (count($data) > 1) {
            $this->log('The data array contains more than 1 element. Lead id ' . $id . ' will be updated', 'Note');
        }
        $lead = $this->getLeadById($id);
        if (!$lead) {
            $this->log('Lead ID ' . $id . ' not found. Stop.');
            return [];
        }
        $updateArray = array_map(function ($el) use ($lead) {
            $el['name'] = empty($el['name']) ? $lead['name'] : $el['name'];
            $el['updated_at'] = time();
            $el['status_id'] = empty($el['status_id']) ? $lead['status_id'] : $el['status_id'];
            if (!empty($el['contacts_id'])) {
                array_push($lead['contacts'], $el['contacts_id']);
                $data['contacts_id'] = $lead['contacts'];
            }
            return $el;
        }, $data);
        $lead['update'] = [reset($updateArray)];
        return $this->request('api/v2/leads', $lead);
    }

    /**
     * Get lead by ID
     * @param integer $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLeadById($id)
    {
        $data = [
            'id' => (int)$id,
        ];
        $lead = $this->request('api/v2/leads', $data, 'GET');
        $lead = !empty($lead['_embedded']['items']) ? $lead['_embedded']['items'] : [];
        return count($lead) == 1 ? reset($lead) : [];
    }

    /**
     * ID key data check
     * @param array $data
     * @param string $requiredKey
     * @return array|false
     * @throws \Exception
     */
    private function checkData(array $data, $requiredKey = 'id')
    {
        $dataClear = array_filter($data, function ($el) use ($requiredKey) {
            return !empty($el[$requiredKey]);
        });
        if (!$dataClear) {
            throw new \Exception('Bad data. Required key ' . $requiredKey
                . ' not exists.', 400);
        }
        return $dataClear;
    }

    /**
     * Writing to the log file
     * @param mixed $data
     * @param string $title
     * @param string $path
     * @return false|int
     */
    public function log($data, $title = '', $path = __DIR__ . '/log.txt')
    {
        $log = "\n------------------------\n";
        $log .= (new \DateTime())->format('d-m-Y H:i:s') . "\n";
        $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
        $log .= print_r($data, 1);
        $log .= "\n------------------------\n";
        return file_put_contents($path, $log, FILE_APPEND);
    }
}