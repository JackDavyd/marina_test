<?php
ini_set('error_log', __DIR__ . '/php_errors_' . date("Y-m-d", time()) . '.log');
ini_set('log_errors', 1);
require __DIR__ . '/vendor/autoload.php';
$amo = new amo\Amo('https://selmagcomsmtp.amocrm.com/');
$task = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

switch ($task) {
    //get token
    //url query = ?action=token
    case 'token':
        $data = [
            "client_secret" => "xxxxxxxxxx",
            "client_id" => "xxxxxxxxx",
            "grant_type" => "authorization_code",
            "code" => 'xxxxxxxx',

            "redirect_uri" => "?action=token",
        ];
        $amo->log($amo->writeToken($data), 'Token');
        break;
        //Request #2
        // url ?action=add_lead
    case 'add_lead':
        $data = [
            [
                'name' => 'New lead from server2',
                'pipeline_id' => 34300186,
                'status_id' => 34300186,
            ]
        ];
        $amo->log($amo->addLeads($data), 'Add lead');
        break;
        //Request #1
        //url ?action=add_contact
    case 'add_contact':
        $data = [
            [
                'name' => 'New contact from server1'
            ]
        ];
        $amo->log($amo->addContacts($data),'Add contact');
        break;
    /**
     * Request #3
    * url query = ?action=link_contact_lead&lead=xxxxx&contact=xxxxxx
    * xxxxx = entity id
    */
    case 'link_contact_lead':
        $lead = filter_input(INPUT_GET, 'lead', FILTER_VALIDATE_INT);
        $contact = filter_input(INPUT_GET, 'contact', FILTER_VALIDATE_INT);
        if (!$lead) {
            throw new \Exception('Not lead ID in $_GET. Example &lead=8673275', 400);
        }
        if (!$contact) {
            throw new \Exception('Not contact ID in $_GET. Example &contact=8673275', 400);
        }
        $contactLink = [
            [
                'id' => $contact,
                'leads_id' => $lead,
                'updated_at' => time(),
            ]
        ];
        $amo->log($amo->updateContacts($contactLink), 'Contact update result');
        $leadLink = [
            [
                'id' => $lead,
                'sale' => 1000,
                'status_id' => 34300174
            ]
        ];
        $amo->log($amo->updateLead($leadLink),'Lead update result');
        break;
    default:
        throw new \Exception('No action in $_GET. Example &action=xxxxx', 400);
}
