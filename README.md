
Install
-------------------
It assumes that git and composer are installed globally. php 7.0+

In the DOCUMENT_ROOT directory, run
````
git clone git@bitbucket.org:JackDavyd/marina_test.git .
composer install
````
Tasks
-------------------
For amo
------------------
In the task
######
Request #1
Create a contact https://www.amocrm.com/developers/content/api/contacts/
######
Request #2
Create a deal in first pipeline and stage and link it to the contact that you previous created 
https://www.amocrm.com/developers/content/api/leads/
######
Request #3
Update the lead stage and any field in the lead (for example lead budget change it 1000$)
######
Done
######
Request #1
######
Create a contact by link http://my-site.com/?action=add_contact
######
Request #2
######
Create a lead by link http://my-site.com/?action=add_lead
######
Request #3
######
Link lead and contact, add lead price, change lead status

link http://my-site.com/?action=link_contact_lead&lead=xxxxx&contact=xxxxx

xxxxx means entity id. &lead=555555&contact=9999999

This is more flexible and does not depend on the order of operations 
request #1 and request #2.

